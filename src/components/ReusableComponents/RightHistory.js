import React, { useContext } from 'react'
import AppContext from '../../store/DataProvider'
import contentType from '../../captionTypes'
import '../ReusableComponentsCss/RightHistory.css'

const RightHistory = () => {
    const context = useContext(AppContext)
    return (
        <div className='right-history-container'>
            <div className="title">Filter</div>
            <select
                value={context.filterCategory}
                id="copy-type"
                name="copy-type"
                onChange={(e) => context.changeFilterCategory(e.target.value)}>
                <option value="all">All</option>
                {
                    contentType.map((data, index) => {
                        return <option key={data.url + index} value={data.title}>{data.title}</option>
                    })
                }
            </select>
        </div>
    )
}

export default RightHistory