import React, { useContext } from 'react'
import '../ReusableComponentsCss/GenerateCopy.css'
import AppContext from '../../store/DataProvider'
import contentType from '../../captionTypes'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const GenerateCopy = () => {

    const context = useContext(AppContext)

    const getTheAppropriateTitleForDescription = () => {
        const filter = contentType.filter(data => data.url === context.contentType)
        return filter[0]
    }

    const generateCaption = () => {
        if (!context.canEdit) return toast("You have view permission only", {
            autoClose: 2000,
            hideProgressBar: false,
        });

        const textAreaTitle = getTheAppropriateTitleForDescription().textAreaTitle
        const textInputTitle = getTheAppropriateTitleForDescription().textInputTitle

        if (textInputTitle === false) {
            if (context.productDescription.trim() === "") return toast(`Hay please add ${textAreaTitle}`, {
                autoClose: 2000,
                hideProgressBar: false,
            });
        } else {
            if (context.productName.trim() === "") return toast(`Hay please add ${textInputTitle}`, {
                autoClose: 2000,
                hideProgressBar: false,
            })
            if (context.productDescription.trim() === "") return toast(`Hay please add ${textAreaTitle}`, {
                autoClose: 2000,
                hideProgressBar: false,
            });
        }


        context.generateTheCaptions()
    }


    const checkInputButtonToActive = () => {
        const textInputTitle = getTheAppropriateTitleForDescription().textInputTitle

        if (textInputTitle === false) {
            if (context.productDescription.trim() === "") return false
            return true
        } else {
            if (context.productName.trim() === "" || context.productDescription.trim() === "") return false
            return true
        }
    }

    const toneChangerData = [
        "Friendly",
        "Luxury",
        "Humorous",
        "Sassy",
        "Informative",
        "Professional",
        "Witty",
        "Funny",
        "Relaxed",
        "Bold",
        "Adventurous",
        "Persuasive",
        "Empathetic",
        "Chill",
        "Excited",
        "Casual",
        "Calm",
        "Sound like Elon Musk",
        "Sound like The Beatles",
        "Thoughtful",
        "Urgent",
        "Appreciative",
        "Assertive",
        "Awestruck",
        "Candid",
        "Worried",
        "Convincing",
        "Cautionary",
        "Compassionate",
        "Critical",
        "Earnest",
        "Enthusiastic",
        "Formal",
        "Humble",
        "Informative",
        "Inspirational",
        "Joyful",
        "Passionate",
        "Comedy",
        "Science Fiction Style",
        "90's Style"
    ]

    const languages = [
        { name: "Amharic", code: "am" },
        { name: "Arabic", code: "ar" },
        { name: "Basque", code: "eu" },
        { name: "Bengali", code: "bn" },
        { name: "Portuguese (Brazil)", code: "pt-BR" },
        { name: "Bulgarian", code: "bg" },
        { name: "Catalan", code: "ca" },
        { name: "Cherokee", code: "chr" },
        { name: "Croatian", code: "hr" },
        { name: "Czech", code: "cs" },
        { name: "Danish", code: "da" },
        { name: "Dutch", code: "nl" },
        { name: "English", code: "en" },
        { name: "Estonian", code: "et" },
        { name: "Filipino", code: "fil" },
        { name: "Finnish", code: "fi" },
        { name: "French", code: "fr" },
        { name: "German", code: "de" },
        { name: "Greek", code: "el" },
        { name: "Gujarati", code: "gu" },
        { name: "Hebrew", code: "iw" },
        { name: "Hindi", code: "hi" },
        { name: "Hungarian", code: "hu" },
        { name: "Icelandic", code: "is" },
        { name: "Indonesian", code: "id" },
        { name: "Italian", code: "it" },
        { name: "Japanese", code: "ja" },
        { name: "Kannada", code: "kn" },
        { name: "Korean", code: "ko" },
        { name: "Latvian", code: "lv" },
        { name: "Lithuanian", code: "lt" },
        { name: "Malay", code: "ms" },
        { name: "Malayalam", code: "ml" },
        { name: "Marathi", code: "mr" },
        { name: "Norwegian", code: "no" },
        { name: "Polish", code: "pl" },
        { name: "Portuguese (Portugal)", code: "pt-PT" },
        { name: "Romanian", code: "ro" },
        { name: "Russian", code: "ru" },
        { name: "Serbian", code: "sr" },
        { name: "Chinese (PRC)", code: "zh-CN" },
        { name: "Slovak", code: "sk" },
        { name: "Slovenian", code: "sl" },
        { name: "Spanish", code: "es" },
        { name: "Swahili", code: "sw" },
        { name: "Swedish", code: "sv" },
        { name: "Tamil", code: "ta" },
        { name: "Telugu", code: "te" },
        { name: "Thai", code: "th" },
        { name: "Chinese (Taiwan)", code: "zh-TW" },
        { name: "Turkish", code: "tr" },
        { name: "Urdu", code: "ur" },
        { name: "Ukrainian", code: "uk" },
        { name: "Vietnamese", code: "vi" },
        { name: "Welsh", code: "cy" }
    ];

    return (
        <div className="generate-copy">
            <label>Copy Type</label>
            <select
                value={context.contentType}
                id="copy-type"
                name="copy-type"
                onChange={(e) => context.changeContentType(e.target.value)}>
                {
                    contentType.map((data, index) => {
                        return <option key={data.url + index} value={data.url}>{data.title}</option>
                    })
                }
            </select>

            {
                context.contentType === "chat" ? <>
                    <label>Models</label>
                    <select
                        value={context.model}
                        id="copy-type"
                        name="copy-type"
                        onChange={(e) => context.changeModel(e.target.value)}>
                        <option value="GPT-3.5 Turbo">GPT-3.5 Turbo</option>
                        <option value="GPT-4o">GPT-4o</option>
                        <option value="LLaMA3 8b">LLaMA3 8b</option>
                        <option value="LLaMA3 70b">LLaMA3 70b</option>
                        <option value="Mixtral 8x7b">Mixtral 8x7b</option>
                        <option value="Gemma 7b">Gemma 7b</option>
                    </select>
                </> : null
            }


            {
                context.contentType === "third/person" ?
                    <React.Fragment>
                        <label>Gender</label>
                        <select
                            value={context.gender}
                            id="copy-type"
                            name="copy-type"
                            onChange={(e) => context.changeGender(e.target.value)}>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                    </React.Fragment> : null
            }

            {
                context.contentType === "tone/changer" ?
                    <React.Fragment>
                        <label for="browser">Select your tone</label>
                        <select
                            style={{ marginBottom: 10 }}
                            value={context.tone}
                            onChange={(e) => context.changeTone(e.target.value)}>
                            {
                                toneChangerData.map((data, index) => {
                                    return <option key={index} value={data}>{data}</option>
                                })
                            }
                        </select>
                    </React.Fragment> : null
            }

            {
                context.contentType !== "chat" ?
                    <>
                        <label>Language Output (Beta)</label>
                        <select
                            value={context.language}
                            id="language"
                            onChange={(e) => context.changeLanguage(e.target.value)}
                            name="language">
                            {
                                languages.map(data => {
                                    return <option key={data.name} value={data.code}>{data.name}</option>
                                })
                            }
                        </select>
                    </> : null
            }

            {
                context.contentType === "advertisement/post/reply" || context.contentType === "advertisement/similar/social/media/post" ?
                    <React.Fragment>
                        <div className="row">
                            <span className='title'>{getTheAppropriateTitleForDescription().textInputTitle}<sup>*</sup></span>
                            <span>{context.productName.length}/400</span>
                        </div>

                        <textarea
                            value={context.productName}
                            maxLength="400"
                            placeholder={getTheAppropriateTitleForDescription().textInputTitlePlaceholder}
                            onChange={(e) => context.changeProductName(e.target.value)} />
                    </React.Fragment> :
                    <React.Fragment>
                        <div className="row" style={getTheAppropriateTitleForDescription().textInputTitle === false ? { display: 'none' } : null}>
                            <span className='title'>{getTheAppropriateTitleForDescription().textInputTitle}<sup>*</sup></span>
                            <span>{context.productName.length}/20</span>
                        </div>

                        <input
                            style={getTheAppropriateTitleForDescription().textInputTitle === false ? { display: 'none' } : null}
                            value={context.productName}
                            maxLength="20"
                            onChange={(e) => context.changeProductName(e.target.value)}
                            placeholder={getTheAppropriateTitleForDescription().textInputTitlePlaceholder} />
                    </React.Fragment>
            }


            <div className="row" style={context.contentType === "chat" ? { display: 'none' } : null}>
                <span className='title'>{getTheAppropriateTitleForDescription().textAreaTitle}<sup>*</sup></span>
                <span>{context.productDescription.length}/400</span>
            </div>

            <textarea
                style={context.contentType === "chat" ? { display: 'none' } : null}
                value={context.productDescription}
                maxLength="400"
                placeholder={getTheAppropriateTitleForDescription().textAreaTitlePlaceholder}
                onChange={(e) => context.changeProductDescription(e.target.value)} />

            <button
                style={context.contentType === "chat" ? { display: 'none' } : null}
                className={checkInputButtonToActive() ? "active" : "inactive"}
                onClick={() => context.captionGerneratingLoading ? null : generateCaption()}>{
                    context.captionGerneratingLoading ? <span className="loader" /> : "Generate AI copy"
                }</button>
        </div>
    )
}

export default GenerateCopy
