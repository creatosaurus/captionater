import React, { useContext } from 'react'
import AppContext from '../../store/DataProvider'
import contentType from '../../captionTypes'
import '../ReusableComponentsCss/RightSaved.css'

const RightSaved = () => {
    const context = useContext(AppContext)

    return (
        <div className="right-saved">
            <div className="title">Saved category</div>

            <select
                value={context.savedCategory}
                id="copy-type"
                name="copy-type"
                onChange={(e) => context.changeContentTypeSaved(e.target.value)}>
                <option value="all">All</option>
                {
                    contentType.map((data, index) => {
                        return <option key={data.url + index} value={data.url}>{data.title}</option>
                    })
                }
            </select>
        </div>
    )
}

export default RightSaved
