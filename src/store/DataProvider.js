import { createContext, useState } from 'react'
import Axios from 'axios'
import constant from '../constant'
import jwt_decode from "jwt-decode";
import copyType from '../captionTypes'

const AppContext = createContext({
    leftSideBarActiveButton: 1,
    leftBarActive: true,
    contentType: "google/ad/headline",
    language: "English",
    productName: "",
    productDescription: "",
    creditRemain: "loading",
    totalCredits: 0,
    createdTillNow: "loading",
    savedCategory: "all",
    filterCategory: "all",
    canEdit: false,
    tone: "",
    workspaceOwnerFeatureFactoryData: null,
    chatQuestion: "",
    model: "GPT-3.5 Turbo",

    //saved state
    savedCaptionsError: false,
    savedPage: 1,
    savedDataFinished: false,
    savedCaptionsLoading: true,
    savedCaptions: [],

    //caption state
    captionGeneratingError: false,
    captionGerneratingLoading: false,
    generatedCaptions: [],
    queryId: null,
    gender: "male",

    //history
    historyData: [],
    historyDataLoading: false,
    historyPage: 1,
    historyDataFinished: false,
    historyDataError: false,
    deleteId: null,
    deleteIndex: null,
    getHistory: () => { },
    updateHistory: () => { },
    deleteHistory: () => { },

    //functions
    generateTheCaptionsFromHistory: () => { },
    changeLeftSideBarActiveButton: () => { },
    getSavedCaptions: () => { },
    changeLeftBarActive: () => { },
    generateTheCaptions: () => { },
    changeContentType: () => { },
    changeLanguage: () => { },
    changeProductName: () => { },
    changeProductDescription: () => { },
    getUserDetails: () => { },
    removeCaptionFromSavedCaptions: () => { },
    changeContentTypeSaved: () => { },
    updateTheGeneratedCaptions: () => { },
    changeGender: () => { },
    changeFilterCategory: () => { },
    getUserFeatureFactoryDetails: () => { },
    changeTone: () => { },
    startChat: () => { },
    changeModel: () => { }
})

export const AppContextProvider = (props) => {

    const [canEdit, setcanEdit] = useState(false)
    const [leftSideBarActiveButton, setleftSideBarActiveButton] = useState(1)
    const [leftBarActive, setleftBarActive] = useState(true)
    const [contentType, setcontentType] = useState("google/ad/headline")
    const [language, setlanguage] = useState("en")
    const [productName, setproductName] = useState("")
    const [productDescription, setproductDescription] = useState("")
    const [creditRemain, setcreditRemain] = useState("loading")
    const [totalCredits, settotalCredits] = useState(0)
    const [createdTillNow, setcreatedTillNow] = useState("loading")
    const [tone, settone] = useState("Friendly")
    const [workspaceOwnerFeatureFactoryData, setWorkspaceOwnerFeatureFactoryData] = useState(null)
    const [model, setModel] = useState("GPT-3.5 Turbo")

    const [savedCategory, setsavedCategory] = useState("all")
    const [filterCategory, setfilterCategory] = useState("all")
    const [chatQuestion, setChatQuestion] = useState("")

    //saved state
    const [savedCaptionsError, setsavedCaptionsError] = useState(false)
    const [savedCaptionsLoading, setsavedCaptionsLoading] = useState(true)
    const [savedDataFinished, setsavedDataFinished] = useState(false)
    const [savedPage, setsavedPage] = useState(1)
    const [savedCaptions, setsavedCaptions] = useState([])
    const [queryId, setqueryId] = useState(null)

    // generate caption
    const [captionGerneratingLoading, setcaptionGerneratingLoading] = useState(false)
    const [captionGeneratingError, setcaptionGeneratingError] = useState(false)
    const [generatedCaptions, setgeneratedCaptions] = useState([])
    const [gender, setgender] = useState("male")

    //history
    const [historyData, sethistoryData] = useState([])
    const [historyPage, sethistoryPage] = useState(1)
    const [historyDataFinished, sethistoryDataFinished] = useState(false)
    const [historyDataLoading, sethistoryDataLoading] = useState(false)
    const [historyDataError, sethistoryDataError] = useState(false)

    const [deleteId, setdeleteId] = useState(null)
    const [deleteIndex, setdeleteIndex] = useState(null)

    const changeModel = (value) => {
        setModel(value)
    }

    const changeFilterCategory = (data) => {
        setfilterCategory(data)
    }

    const getHistory = async () => {
        try {
            sethistoryDataLoading(true)
            sethistoryDataError(false)
            const token = localStorage.getItem('token')
            const organizationId = localStorage.getItem('organizationId')

            const config = { headers: { "Authorization": `Bearer ${token}` } }
            const res = await Axios.get(`${constant.url}history/${organizationId}?page=${historyPage}`, config)

            if (res.data.data.length < 10) sethistoryDataFinished(true)

            sethistoryPage((prev) => prev + 1)
            sethistoryData((prev) => [...prev, ...res.data.data])
            sethistoryDataLoading(false)
            sethistoryDataError(false)
        } catch (error) {
            sethistoryDataLoading(false)
            sethistoryDataError(true)
        }
    }

    const updateHistory = (data) => {
        sethistoryData((prev) => [data, ...prev])
    }

    const deleteHistory = async (id, index) => {
        try {
            const token = localStorage.getItem('token')
            setdeleteId(id)
            setdeleteIndex(index)
            let res = await Axios.delete(constant.url + 'history', {
                data: { id: id, index: index },
                headers: { "Authorization": `Bearer ${token}` }
            })

            //check the document removed or ans removed
            if (res.data === "removed document") {
                let filterHistory = historyData.filter(data => data._id !== id)
                sethistoryData(filterHistory)
            } else {
                let filterHistory = historyData.map(data => {
                    if (data._id === id) {
                        data.answer = data.answer.filter((data, i) => i !== index)
                    }
                    return data
                })
                sethistoryData(filterHistory)
            }

            setdeleteId(null)
            setdeleteIndex(null)
        } catch (error) {
            setdeleteId(null)
            setdeleteIndex(null)
        }
    }

    const changeGender = (value) => {
        setgender(value)
    }

    const changeLeftSideBarActiveButton = (value) => {
        setleftSideBarActiveButton(value)
    }

    const changeContentTypeSaved = (value) => {
        setsavedCategory(value)
    }


    const changeLeftBarActive = () => setleftBarActive((prev) => !prev)
    const changeContentType = (value) => setcontentType(value)
    const changeLanguage = (value) => setlanguage(value)
    const changeProductName = (value) => setproductName(value)
    const changeProductDescription = (value) => setproductDescription(value)

    const getUserDetails = async () => {
        try {
            const token = localStorage.getItem('token')
            const organizationId = localStorage.getItem('organizationId')

            const config = { headers: { "Authorization": `Bearer ${token}` } }
            const res = await Axios.get(constant.url + '/info/' + organizationId, config)
            setcreatedTillNow(res.data.createdTillNow)
        } catch (error) {
            console.log(error)
        }
    }

    const getUserFeatureFactoryDetails = async () => {
        try {
            const token = localStorage.getItem('token')
            const config = { headers: { "Authorization": `Bearer ${token}` } }
            const res = await Axios.get(`https://api.app.creatosaurus.io/creatosaurus/user/info`, config)

            let userDetails = res.data.activeWorkspace.team.filter(data => data.user_email === res.data.userData.email)

            if (userDetails[0].role !== "view") {
                setcanEdit(true)
            } else {
                setcanEdit(false)
            }

            setWorkspaceOwnerFeatureFactoryData(res.data.workspaceOwnerFeatureFactoryData)
            let numberOfUsers = res.data.workspaceOwnerFeatureFactoryData.users === "unlimited" ? 1 : parseInt(res.data.workspaceOwnerFeatureFactoryData.users)
            settotalCredits(res.data.workspaceOwnerFeatureFactoryData.planId.aiCredites * numberOfUsers)
            setcreditRemain(res.data.workspaceOwnerFeatureFactoryData.aiCredites)
            localStorage.setItem("organizationId", res.data.activeWorkspace._id)
            localStorage.setItem("organizationName", res.data.activeWorkspace.workspace_name)
            getSavedCaptions()
            getUserDetails()
            getHistory()
        } catch (error) {
            console.log(error.response.data.error)
        }
    }

    const generateTheCaptions = async () => {
        try {
            setChatQuestion("")
            setcaptionGerneratingLoading(true)
            setcaptionGeneratingError(false)

            // get the information of copy type
            let data = copyType.filter(data => data.url === contentType)

            const token = localStorage.getItem('token')
            const organizationId = localStorage.getItem('organizationId')
            const decoded = jwt_decode(token);

            const config = { headers: { "Authorization": `Bearer ${token}` } }
            let url = constant.url + contentType

            const response = await Axios.post(url, {
                organizationId: organizationId,
                workspaceOwnerUserId: workspaceOwnerFeatureFactoryData.userId,
                userId: decoded.id,
                userName: decoded.userName,
                companyName: data[0].textInputTitle === false ? null : productName,
                companyDescription: productDescription,
                gender: gender,
                copyType: data[0].title,
                language: language,
                tone: tone
            }, config)

            setgeneratedCaptions(response.data.data)
            setqueryId(response.data.queryId)
            updateHistory(response.data.historyData)
            setcaptionGerneratingLoading(false)
            setcreatedTillNow((prev) => prev + 1)
            if (creditRemain === "unlimited") return
            setcreditRemain((prev) => parseInt(prev) - 1)
        } catch (error) {
            setcaptionGerneratingLoading(false)
            setcaptionGeneratingError(error.response?.status)
        }
    }


    const generateTheCaptionsFromHistory = async (historyURL, historyProductName, historyProductDescription, historyLanguage) => {
        try {
            setChatQuestion("")
            setcaptionGerneratingLoading(true)
            setcaptionGeneratingError(false)

            // get the information of copy type
            let data = copyType.filter(data => data.url === historyURL)

            const token = localStorage.getItem('token')
            const organizationId = localStorage.getItem('organizationId')
            const decoded = jwt_decode(token);

            const config = { headers: { "Authorization": `Bearer ${token}` } }
            let url = constant.url + historyURL

            const response = await Axios.post(url, {
                organizationId: organizationId,
                workspaceOwnerUserId: workspaceOwnerFeatureFactoryData.userId,
                userId: decoded.id,
                userName: decoded.userName,
                companyName: data[0].textInputTitle === false ? null : historyProductName,
                companyDescription: historyProductDescription,
                gender: gender,
                copyType: data[0].title,
                language: historyLanguage,
                tone: tone
            }, config)

            setgeneratedCaptions(response.data.data)
            setqueryId(response.data.queryId)
            updateHistory(response.data.historyData)
            setcaptionGerneratingLoading(false)
            setcreatedTillNow((prev) => prev + 1)
            if (creditRemain === "unlimited") return
            setcreditRemain((prev) => parseInt(prev) - 1)
        } catch (error) {
            setcaptionGerneratingLoading(false)
            setcaptionGeneratingError(error.response?.status)
        }
    }

    const updateTheGeneratedCaptions = (data) => {
        setsavedCaptions((prev) => [data, ...prev])
    }

    const getSavedCaptions = async () => {
        try {
            setsavedCaptionsLoading(true)
            setsavedCaptionsError(false)
            const token = localStorage.getItem('token')
            const organizationId = localStorage.getItem('organizationId')

            const config = { headers: { "Authorization": `Bearer ${token}` } }
            const res = await Axios.get(`${constant.url}saved/${organizationId}?page=${savedPage}`, config)

            if (res.data.length < 10) setsavedDataFinished(true)
            setsavedCaptions((prev) => [...prev, ...res.data])
            setsavedPage((prev) => prev + 1)
            setsavedCaptionsLoading(false)
        } catch (error) {
            setsavedCaptionsLoading(false)
            setsavedCaptionsError(true)
        }
    }

    const removeCaptionFromSavedCaptions = async (id) => {
        let filterData = savedCaptions.filter(data => data._id !== id)
        setsavedCaptions(filterData)
    }

    const changeTone = (value) => {
        settone(value)
    }

    const startChat = async (message) => {
        try {
            setcaptionGerneratingLoading(true)
            setcaptionGeneratingError(false)
            const token = localStorage.getItem('token')
            const config = { headers: { "Authorization": `Bearer ${token}` } }
            const organizationId = localStorage.getItem('organizationId')
            const decoded = jwt_decode(token);

            setChatQuestion(message)
            // get the information of copy type
            let data = copyType.filter(data => data.url === "chat")

            const res = await Axios.post(constant.url + "chat", {
                message: message,
                organizationId: organizationId,
                workspaceOwnerUserId: workspaceOwnerFeatureFactoryData.userId,
                userId: decoded.id,
                userName: decoded.userName,
                companyName: data[0].textInputTitle === false ? null : productName,
                companyDescription: productDescription,
                gender: gender,
                copyType: data[0].title,
                language: language,
                tone: tone
            }, config)

            setChatQuestion(res.data.historyData.query)
            setgeneratedCaptions([{
                text: res.data.data
            }])
            setcaptionGerneratingLoading(false)
        } catch (error) {
            setcaptionGeneratingError(true)
            setcaptionGerneratingLoading(false)
        }
    }

    const context = {
        //state
        leftSideBarActiveButton: leftSideBarActiveButton,
        leftBarActive: leftBarActive,
        contentType: contentType,
        language: language,
        productName: productName,
        productDescription: productDescription,
        creditRemain: creditRemain,
        totalCredits: totalCredits,
        createdTillNow: createdTillNow,
        savedCategory: savedCategory,
        filterCategory: filterCategory,
        tone: tone,
        workspaceOwnerFeatureFactoryData: workspaceOwnerFeatureFactoryData,
        chatQuestion: chatQuestion,
        model: model,

        //saved state
        savedCaptionsError: savedCaptionsError,
        savedCaptionsLoading: savedCaptionsLoading,
        savedCaptions: savedCaptions,
        savedDataFinished: savedDataFinished,
        savedPage: savedPage,

        // caption state
        captionGeneratingError: captionGeneratingError,
        captionGerneratingLoading: captionGerneratingLoading,
        generatedCaptions: generatedCaptions,
        queryId: queryId,
        gender: gender,

        //history
        historyData: historyData,
        historyDataLoading: historyDataLoading,
        historyDataError: historyDataError,
        historyPage: historyPage,
        historyDataFinished: historyDataFinished,
        deleteId: deleteId,
        deleteIndex: deleteIndex,
        getHistory: getHistory,
        updateHistory: updateHistory,
        deleteHistory: deleteHistory,
        canEdit: canEdit,

        //functions
        changeModel: changeModel,
        changeLeftSideBarActiveButton: changeLeftSideBarActiveButton,
        getSavedCaptions: getSavedCaptions,
        changeLeftBarActive: changeLeftBarActive,
        generateTheCaptions: generateTheCaptions,
        changeContentType: changeContentType,
        changeLanguage: changeLanguage,
        changeProductName: changeProductName,
        changeProductDescription: changeProductDescription,
        getUserDetails: getUserDetails,
        removeCaptionFromSavedCaptions: removeCaptionFromSavedCaptions,
        changeContentTypeSaved: changeContentTypeSaved,
        updateTheGeneratedCaptions: updateTheGeneratedCaptions,
        changeGender: changeGender,
        changeFilterCategory: changeFilterCategory,
        getUserFeatureFactoryDetails: getUserFeatureFactoryDetails,
        changeTone: changeTone,
        startChat: startChat,
        generateTheCaptionsFromHistory: generateTheCaptionsFromHistory
    }

    return <AppContext.Provider value={context}>
        {props.children}
    </AppContext.Provider>
}

export default AppContext